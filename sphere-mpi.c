#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <mpi.h>


/*
	La funcion sphere se define como f(x) = Sumatorio(i, N) -> ( x_i ^ 2) , para i = 0 ... N
*/
#define POBLACION 11                        //numero de individuos para facilitar los cruces
#define LONG_COD 20								  //numero de bits utilizados para el genotipo
#define LIMITE -5.12								  //limite de valores entre los que oscilara la funcion sphere
#define PROB_CRUCE 0.3                      //autoexplicativo, probabilidad de cruce entre dos individuos
#define PROB_MUTACION 0.001                 //autoexplicativo, probabilidad de mutacion de un individuo
#define INTERVALO 10.24/pow(2,LONG_COD/2)

#define EPOCH 1000	//Este parametro es una barbaridad para que se dispare el tiempo de la funcion de fitness
#define COMPR 10		//Numero de iteraciones tras las que se ejecuta el reduce para ver si existe solucion
#define STOP 1			//Este parametro actua como señal de mensaje para que si esta en el buffer de los procesos estos aborten su ejecucion.
#define CONT 0			//Este parametro indica que continuen su ejecucion.

/*Estructura de datos que representa a un individuo, o posible solucion del problema*/
typedef struct {
	int genotipo[LONG_COD];
	double aptitud;
} Individuo;

void decoder (double *, double *, int *);
double fitness (double, double);
int generarBinario (void);
Individuo generarIndividuo (void);
Individuo * generarPoblacion (void);
Individuo * seleccionTorneos(Individuo * pob);
void mutacionHijos (Individuo *);
void cruzarSeleccion (Individuo *);
Individuo elite(Individuo *);
void AG();
void imprimePoblacion (Individuo *);
void imprimeGenotipo(Individuo);
void generarGraficoGeneracional(void);

int 
main(int argc, char *argv[]) 
{
	MPI_Init(&argc, &argv); //Inicializa las rutinas y el entorno MPI
  	srand(time(NULL));
	AG(); 
	MPI_Finalize();
	return 0;
}

/* PROC decoder (double *x, double *y, int *genotipo)						DEV (double)
 * MODIFICA (double *x double *y)
 * EFECTO recibe un vector de enteros compuesto de 0 y 1 que representa dos numeros
 * 	codificados en binario. se encarga de convertir estos dos numeros binarios a su
 * 	equivalente en decimal con ayuda de la macro PRECISION (incremento del valor entre
 * 	cada binario) y la macro LIMITE que es el valor del limite inferior de la repre-
 * 	sentacion que en el problema es -5.12	*/

void decoder (double * x, double * y, int * genotipo)
{
	int i;
	*x = *y = 0.0;

	// calculo del primer decimal
	for(i=0; i<LONG_COD/2; i++)
		*x += genotipo[i] * pow(2, (LONG_COD/2)-(i+1));
	*x = (*x) * INTERVALO + LIMITE;

	//calculo del segundo decimal
	for(;i<LONG_COD;i++)
		*y += genotipo[i] * pow(2, LONG_COD-(i+1));
	*y = (*y) * INTERVALO + LIMITE;
}

/* PROC fitness (double p1, double p2)					DEV (double)
 * MODIFICA nada
 * EFECTO recibe dos valores que representan los puntos que caracterizan a un individuo
 * 	la funcion sirve para calcular la aptitud o fitness de cierto individuo segun sus
 * 	puntos. este valor de aptitud es el que devuelve la funcion. */

double fitness (double p1, double p2)
{
   double result;
   int i, j;
   for (i = 0; i<EPOCH; i++ ) {
   	for (j = 0; j<EPOCH; j++ ) {
      result = pow(p1,2) + pow(p2,2);
    }
   }
   return result;
}

/* PROC generarBinario (void)								DEV (void)
 * MODIFICA nada
 * EFECTO se encarga de devolver un valor entero que siempre sera cero o uno. lo vamos a
 * 	utilizar para generar los individuos al principio dado que su genoma es una cadena
 * 	binaria que se genera aleatoriamente	*/

int generarBinario (void) {
	if (1 + (int) (10.0*rand()/(RAND_MAX+1.0)) > 5)
		return 1;
	else
		return 0;
}

/* PROC generarIndividuo (void)							DEV (Individuo)
 * MODIFICA nada
 * EFECTO se encarga de generar un individuo utilizando valores aleatorios. primero crea
 * 	la cadena de bits del genotipo utilizando la funcion generaBinario y luego evalua
 * 	la aptitud del individuo utilizando la funcion decoder para decodificar el genotipo
 * 	y la funcion fitness para obtener la aptitud.	*/

Individuo generarIndividuo (void){
	Individuo ind;
	int i;
	double x, y;

	for (i=0; i<LONG_COD; i++)
		ind.genotipo[i]=generarBinario();

	decoder(&x, &y, ind.genotipo); //guarda en x, y el punto correspondiente a decodificar un genotipo de 20 bits : 0010101...100 en (-4, 3.1) por ejemplo
	ind.aptitud = fitness(x,y);

	return ind;
}

/* PROC generarPoblacion (void)							DEV (Individuo *)
 * MODIFICA nada
 * EFECTO esta funcion genera una poblacion con la cantidad de individuos dada por la
 * 	macro POBLACION. para generar cada individuo utiliza la funcion generarIndividuo()
 * 	y una vez ha terminado el bucle, devuelve el puntero al primer individuo	*/

Individuo * generarPoblacion(void)
{
	Individuo * poblacion;
	int i;

	poblacion = (Individuo *) malloc(sizeof(Individuo)*POBLACION);
	for(i=0;i<POBLACION;i++)
		poblacion[i] = generarIndividuo();

	return poblacion;
}

/* PROC seleccionTorneos (Individuo *)					DEV (Individuo *)
 * MODIFICA nada
 * EFECTO se crea un nuevo vector de individuos que contendra a los individuos seleccionados
 * 	para el cruce. la seleccion se hace por torneos por tanto cada individuo seleccionado
 * 	sera el que tenga mejor aptitud de dos candidatos. el proceso se repite en POBLACION-1 (HASTA POBLACION-2)
 * 	iteraciones, que es la cantidad de individuos que se deben seleccionar. la reserva de
 * 	memoria se hace sobre POBLACION individuos dado que, como luego vamos a seleccionar el
 * 	mejor de la poblacion anterior por elitismo, debemos dejar una plaza de la siguiente
 * 	generacion libre. la seleccion es con repeticion */

Individuo * seleccionTorneos (Individuo * poblacion)
{
	Individuo candidato_a, candidato_b;
	int i;

	/*Crea un vector de identico tamanho al de la poblacion.
	  Tener en cuenta que pueden existir individuos repetidos si compiten A,B  y A,C que ganen ambos A.
	  Al escoger las posiciones de los individuos tambien aleatoriamente se pueden dar casos en que un individuo compita contra si mismo, (A,A)
	  o que un mismo torneo se repita dos veces por haber salido el par (posicion_y, posicion_x) repetido.
	*/
	Individuo * seleccion = (Individuo *) malloc (sizeof(Individuo)*POBLACION);

	//Se itera hasta el penultimo elemento de la generacion, dejando el ultimo para el ganador por elitismo
	for (i=0; i<POBLACION-1; i++)
	{
		candidato_a = poblacion[(int) (((double) POBLACION)*rand()/(RAND_MAX+1.0))];
		candidato_b = poblacion[(int) (((double) POBLACION)*rand()/(RAND_MAX+1.0))];

		if (candidato_a.aptitud < candidato_b.aptitud)
			seleccion[i] = candidato_a;
		else
			seleccion[i] = candidato_b;
	}

	return seleccion;
}

/* PROC mutacionHijos (Individuo *)							DEV (void)
 * MODIFICA (Individuo *)
 * EFECTO esta funcion aplica la mutacion segun la probabilidad dada por PROB_MUTACION.
 * 	recibe un vector de individuos en el que debe ocurrir que los dos primeros sean
 * 	los hijos correspondientes a un cruce. el genotipo de cada uno de los individuos
 * 	se recorre por completo calculando la probabilidad de que el bit mute y cambiando
 * 	si se diera el caso positivo	*/

void mutacionHijos (Individuo * hijos)
{
	int i, j;

	for(i=0; i<2; i++)
		for(j=0; j<LONG_COD; j++)
			if ((double) rand()/(RAND_MAX+1.0) < PROB_MUTACION)
			{
				if(hijos[i].genotipo[j])
					hijos[i].genotipo[j] = 0;
				else hijos[i].genotipo[j] = 1;
			}
}

/* PROC cruzarSeleccion (Individuo *)					DEV (void)
 * MODIFICA (Individuo *)
 * EFECTO esta funcion se encarga de cruzar los individuos seleccionados. la seleccion
 * 	esta ordenada luego cruzamos los individuos seguidos de dos en dos. para cada una
 * 	de las iteraciones se aplica la probabilidad de cruce. en caso de que se crucen
 * 	los individuos se genera un punto de cruce y se intercambian las porciones del
 * 	vector. luego se llama a la funcion de mutacion. en caso de que no haya cruce, los
 * 	padres pasan directamente a la siguiente generacion	*/

void cruzarSeleccion (Individuo * seleccion)
{
	int i, j, punto, aux;
	double x, y;

	/*Se sigue iterando hasta el penultimo elemento del vector seleccion*/
	for(i=0; i<POBLACION-1; i+=2)
	{
		if((double) rand()/(RAND_MAX+1.0) < PROB_CRUCE)
		{
			/*Se escoge un punto aleatorio dentro de el vector del genotipo*/
			punto = (int) (((double) LONG_COD)*rand()/(RAND_MAX+1.0));
	
			/*Desde ese punto hasta el final del vector se intercambian todas las posiciones entre los dos genotipos de los individuos */
			for(j=punto; j<LONG_COD; j++)
			{
				aux=seleccion[i].genotipo[j];
				seleccion[i].genotipo[j]=seleccion[i+1].genotipo[j];
				seleccion[i+1].genotipo[j]=aux;
			}

			/*Se llama a la funcion de mutacion para el individuo*/
			mutacionHijos(&seleccion[i]);

			/*Se obtiene la nueva aptitud para los dos nuevos individuos de la seleccion*/
			decoder(&x, &y, seleccion[i].genotipo);
			seleccion[i].aptitud = fitness(x,y);

			decoder(&x, &y, seleccion[i+1].genotipo);
			seleccion[i+1].aptitud = fitness(x,y);
		}
	}
}

/* PROC elite (Individuo * poblacion)					DEV (Individuo)
 * MODIFICA nada
 * EFECTO se trata de una funcion que devuelve el mejor individuo de una poblacion
 * 	que se pasa como argumento. utiliza un individuo como comparador y devuelve
 * 	el que para nuestro caso tiene el mejor fitness, es decir, aptitud mas baja */

Individuo elite (Individuo * poblacion)
{
	int i;
	Individuo best = poblacion[0];

	for(i=0; i<POBLACION; i++)
		if(best.aptitud > poblacion[i].aptitud)
			best = poblacion[i];

	return best;
}

/* PROC AG(void)												DEV (void)
 * MODIFICA nada
 * EFECTO se trata de la funcion que ejecuta el algoritmo. el proceso es el siguiente
 * 	1 - Generar la poblacion
 * 	2 - Seleccion de candidatos al cruce
 * 	3 - Cruce de los candidatos (incluye mutacion)
 * 	4 - Incluir al mejor de la generacion anterior en la nueva
 * 	5 - Repetir el proceso	*/

void AG (void)
{
	Individuo * seleccion, * poblacion = generarPoblacion();
	Individuo best;
	int generacion = 0;
	double x, y, aptitud_final;
	int mensaje, bufenv = CONT, bufrec;
	int nprocs, myid, size;
	MPI_Comm_rank(MPI_COMM_WORLD, &myid); //Asigna en myid el indicador del proceso dentro del comunicador, es decir del conjunto de procesos en el que se encuentra.
  MPI_Comm_size(MPI_COMM_WORLD, &size); //Guarda en size el numero de procesos dentro del comunicador.	
	MPI_Request request;
	MPI_Status status;

	do
	{
		seleccion = seleccionTorneos(poblacion);
		cruzarSeleccion(seleccion);
		seleccion[POBLACION-1] = elite(poblacion); //En la ultima posicion del vector de seleccion se coloca el individuo de mejor aptitud de la poblacion original
		free(poblacion);
		poblacion = seleccion; //Ahora la nueva poblacion es la creada con seleccion (torneos, cruce, mutacion y elite)

		if (elite(poblacion).aptitud < pow(10,-2)){ //Si hay solucion, se indica señal de STOP y sale del bucle
			bufenv = STOP;
			break;
		}	

		if ((generacion % COMPR) == 0) //Cada 10 iteraciones en este caso, se hace una reduccion de la operacion OR sobre todos los procesos al que llama.
			MPI_Reduce(&bufenv, &bufrec, 1, MPI_INT, MPI_LOR, myid, MPI_COMM_WORLD);
		
		if (bufrec == 1) //Si es igual a 1, significa que al menos uno de ellos encontró solución, y aborta la ejecución saliendo del bucle.
			break;

		generacion++;
	} while (elite(poblacion).aptitud > pow(10,-2));

	best = elite(poblacion); //Se escoge al mejor de la poblacion final como solucion(si no encontró solucion da igual, en el reduce del elite quedará descartado).

	if (myid == 0) { //Se hace el reduce del elite en el proceso raiz de todos.
		MPI_Reduce(&best.aptitud, &aptitud_final, 1, MPI_DOUBLE, MPI_MIN, 0, MPI_COMM_WORLD);
	}

	free(poblacion);
	decoder(&x, &y, best.genotipo);

	printf ("*************************************\n");
	printf ("*          FIN DEL ALGORITMO        *\n");
	printf ("*************************************\n");
	printf (" - En el punto (%.5f, %.5f)\n", x, y);
	printf (" - Su fenotipo es %.5f\n", aptitud_final);
	printf (" - Es la generacion numero %i\n", generacion);
	printf ("*************************************\n");
	MPI_Finalize();
}
